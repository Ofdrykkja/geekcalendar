package com.sweetrenard.geekcalendar.widget;

import android.content.Context;
import android.util.MonthDisplayHelper;
import android.util.TypedValue;
import android.widget.TextView;

public class WeekCell extends TextView {

	public WeekCell(Context context) {
		super(context);
		init(context);
	}
	
	private void init(Context context) {
		//setWidth(CalendarLayout.PXtoDP(context, 30));
		setHeight(CalendarLayout.PXtoDP(context, 20));
	}
	
	private static boolean isWeekend(int column) {
		return (column == 5 || column == 6);
	}
	
	public static WeekCell getInstance(
			Context context, MonthDisplayHelper helper, int column, CalendarLayout.Params params) {
		WeekCell wc = new WeekCell(context);
		//wc.setWidth(CalendarLayout.PXtoDP(context, params.COLUMN_WIDTH));
		wc.setWidth((int)params.COLUMN_WIDTH);
		if (isWeekend(column)) {
			wc.setTextColor(params.WEEKEND_COLOR);
			wc.setBackgroundResource(params.WEEKEND_BACKGROUND);
		} else {
			wc.setTextColor(params.WEEK_COLOR);
			wc.setBackgroundResource(params.WEEK_BACKGROUND);
		}
		wc.setTypeface(params.WEEK_FONT);
		wc.setTextSize(TypedValue.COMPLEX_UNIT_PX, params.WEEK_SIZE);
		wc.setText(String.format("%3s", Integer.toBinaryString(column)).replace(' ', '0'));	
		return wc;
	}
}
