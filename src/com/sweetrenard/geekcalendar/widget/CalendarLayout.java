package com.sweetrenard.geekcalendar.widget;

import java.util.Calendar;

import com.sweetrenard.geekcalendar.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.widget.GridLayout;
import android.widget.TextView;

public class CalendarLayout extends GridLayout {
	
	private Calendar current_date = null;
	private MonthDisplayHelper helper = null;
	private static final int GRID_X = 6;
	private static final int GRID_Y = 7;
	private static final int PADDING = 8;
	private static final String TAG = "#CalendarLayout#";
	
	public CalendarLayout(Context context) {
		this(context, null);
	}
	
	public CalendarLayout(Context context, AttributeSet attrs) {
		this(context, attrs, R.style.GeekCalendarTheme_Dark_Widget);
	}
	
	public CalendarLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init(context, attrs, defStyle);
	}
	
	private void init(Context context, AttributeSet attrs, int defStyle) {
		Log.d(TAG, "[INIT]");
		current_date = Calendar.getInstance();
		helper = new MonthDisplayHelper(
				current_date.get(Calendar.YEAR),
				current_date.get(Calendar.MONTH),
				Calendar.MONDAY);
		TypedArray a = context.getTheme().obtainStyledAttributes(
				attrs, R.styleable.CalendarLayout,
				0, defStyle);
		Params params = new Params(context, a);
		
		setBackgroundResource(params.WIDGET_BACKGROUND);
		
		//int dp_padding = PXtoDP(context, PADDING);
		int P = (int)params.WIDGET_PADDING;
		setPadding(P, P, P, P);
		
		setColumnCount(GRID_Y);
		addView(MonthCell.getInstance(context, helper, params));
		for (int i = 0; i < GRID_Y; i++) {
			addView(WeekCell.getInstance(context, helper, i, params));
		}
        for (int i = 0; i < GRID_X; i++) {
            for (int j = 0; j < GRID_Y; j++) {
            	if (helper.isWithinCurrentMonth(i, j)) {
	            	addView(DayCell.getInstance(context, helper, i, j, params));
            	} else {
            		addView(new TextView(context));
            	}
            }
        }
	}
	
	public static int PXtoDP(Context context, float PX) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return ((int) (PX * scale + 0.5f));
	}
	
	public static class Params {
		public int MONTH_COLOR;
		public float MONTH_SIZE;
		public Typeface MONTH_FONT;
		public int MONTH_BACKGROUND;
		public int WEEK_COLOR;
		public float WEEK_SIZE;
		public Typeface WEEK_FONT;
		public int WEEK_BACKGROUND;
		public int WEEKEND_COLOR;
		public int WEEKEND_BACKGROUND;
		public int DAY_COLOR;
		public float DAY_SIZE;
		public Typeface DAY_FONT;
		public int DAY_BACKGROUND;
		public int HOLIDAY_COLOR;
		public int HOLIDAY_BACKGROUND;
		public int CURRENT_DAY_COLOR;
		public int CURRENT_DAY_BACKGROUND;
		public int CURRENT_DAY_HOLIDAY_COLOR;
		public int CURRENT_DAY_HOLIDAY_BACKGROUND;
		public int WIDGET_BACKGROUND;
		public float COLUMN_WIDTH;
		public float WIDGET_PADDING;
		
		private Context context;
		private TypedArray a;
		
		public Params(Context context, TypedArray a) {
			this.context = context;
			this.a = a;
			try {
				MONTH_COLOR = a.getColor(R.styleable.CalendarLayout_monthColor, 0);
				MONTH_SIZE = a.getDimensionPixelSize(R.styleable.CalendarLayout_monthSize, -1);
				MONTH_FONT = getTypeface(R.styleable.CalendarLayout_monthFont);
				MONTH_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_monthBackground, 0);
				WEEK_COLOR = a.getColor(R.styleable.CalendarLayout_weekColor, 0);
				WEEK_SIZE = a.getDimensionPixelSize(R.styleable.CalendarLayout_weekSize, -1);
				WEEK_FONT = getTypeface(R.styleable.CalendarLayout_weekFont);
				WEEK_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_weekBackground, 0);
				WEEKEND_COLOR = a.getColor(R.styleable.CalendarLayout_weekendColor, 0);
				WEEKEND_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_weekendBackground, 0);
				DAY_COLOR = a.getColor(R.styleable.CalendarLayout_dayColor, 0);
				DAY_SIZE = a.getDimensionPixelSize(R.styleable.CalendarLayout_daySize, -1);
				DAY_FONT = getTypeface(R.styleable.CalendarLayout_dayFont);
				DAY_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_dayBackground, 0);
				HOLIDAY_COLOR = a.getColor(R.styleable.CalendarLayout_holidayColor, 0);
				HOLIDAY_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_holidayBackground, 0);
				CURRENT_DAY_COLOR = a.getColor(R.styleable.CalendarLayout_currentDayColor, 0);
				CURRENT_DAY_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_currentDayBackground, 0);
				CURRENT_DAY_HOLIDAY_COLOR = a.getColor(R.styleable.CalendarLayout_currentDayHolidayColor, 0);
				CURRENT_DAY_HOLIDAY_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_currentDayHolidayBackground, 0);
				WIDGET_BACKGROUND = a.getResourceId(R.styleable.CalendarLayout_widgetBackground, 0);
				COLUMN_WIDTH = a.getDimensionPixelSize(R.styleable.CalendarLayout_columnWidth, -1);
				WIDGET_PADDING = a.getDimensionPixelSize(R.styleable.CalendarLayout_widgetPadding, 0);
			} finally {
				a.recycle();
			}
		}
		
		private Typeface getTypeface(int f) {
			return Typeface.createFromAsset(context.getAssets(), a.getString(f));
		}
	}
  
}