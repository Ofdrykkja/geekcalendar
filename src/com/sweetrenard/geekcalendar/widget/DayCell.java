package com.sweetrenard.geekcalendar.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import android.content.Context;
import android.util.MonthDisplayHelper;
import android.util.TypedValue;
import android.widget.TextView;

public class DayCell extends TextView {

	private static final class Holiday {
		public int month;
		public int day;
		
		public Holiday(int month, int day) {
			this.month = month;
			this.day = day;
		}
		
		@Override
		public boolean equals(Object o) {
			return (o instanceof Holiday &&
					((Holiday)o).day == this.day &&
					((Holiday)o).month == this.month);
			
		}
	}
	
	private static ArrayList<Holiday>HOLIDAYS = new ArrayList<Holiday>(Arrays.asList(
			new Holiday(0, 1), new Holiday(0, 2), new Holiday(0, 3),
			new Holiday(0, 4), new Holiday(0, 5), new Holiday(0, 6),
			new Holiday(0, 7), new Holiday(0, 8), 
			new Holiday(1, 23), new Holiday(2, 8),
			new Holiday(4, 1), new Holiday(4, 2), new Holiday(4, 3),
			new Holiday(4, 9), new Holiday(4, 10),
			new Holiday(5, 12), new Holiday(6, 4), new Holiday(10, 4)
	));
	
	public DayCell(Context context) {
		super(context);
		init(context);
	}
	
	private void init(Context context) {
		//setWidth(CalendarLayout.PXtoDP(context, 30));
		setHeight(CalendarLayout.PXtoDP(context, 27));
	}
	
	private static boolean isHoliday(MonthDisplayHelper helper, int day) {
		int[] rows = helper.getDigitsForRow(helper.getRowOf(day));
		int month = helper.getMonth();
		return ((day == rows[5] || day == rows[6]) || HOLIDAYS.contains(new Holiday(month, day)));
	}
	
	private static boolean isCurrentDay(MonthDisplayHelper helper, int day) {
		int month = helper.getMonth();
		int year = helper.getYear();
		Calendar calendar = Calendar.getInstance();
		int current_day = calendar.get(Calendar.DAY_OF_MONTH);
		int current_month = calendar.get(Calendar.MONTH);
		int current_year = calendar.get(Calendar.YEAR);
		
		return (year == current_year && month == current_month && day == current_day);
	}
	
	public static DayCell getInstance(
			Context context, MonthDisplayHelper helper,
			int row, int column, CalendarLayout.Params params) {
		DayCell dc = new DayCell(context);
		int day = helper.getDayAt(row, column);
		//dc.setWidth(CalendarLayout.PXtoDP(context, params.COLUMN_WIDTH));
		dc.setWidth((int)params.COLUMN_WIDTH);
		dc.setTypeface(params.DAY_FONT);
		dc.setTextSize(TypedValue.COMPLEX_UNIT_PX, params.DAY_SIZE);
		if (isHoliday(helper, day)) {
			if (isCurrentDay(helper, day)) {
				dc.setTextColor(params.CURRENT_DAY_HOLIDAY_COLOR);
				dc.setBackgroundResource(params.CURRENT_DAY_HOLIDAY_BACKGROUND);
			} else {
				dc.setTextColor(params.HOLIDAY_COLOR);
				dc.setBackgroundResource(params.HOLIDAY_BACKGROUND);
			}
		} else {
			if (isCurrentDay(helper, day)) {
				dc.setTextColor(params.CURRENT_DAY_COLOR);
				dc.setBackgroundResource(params.CURRENT_DAY_BACKGROUND);
			} else {
				dc.setTextColor(params.DAY_COLOR);
				dc.setBackgroundResource(params.DAY_BACKGROUND);
			}
		}
		dc.setText(String.format("%02X", day));
		return dc;
	}

}
