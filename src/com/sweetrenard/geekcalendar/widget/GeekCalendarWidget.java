package com.sweetrenard.geekcalendar.widget;

import com.sweetrenard.geekcalendar.R;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

public class GeekCalendarWidget extends AppWidgetProvider {
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.main);
		//CalendarLayout calendar = new CalendarLayout(context, null, R.style.GeekCalendarTheme_Dark_WidgetTransparent);
		CalendarLayout calendar = new CalendarLayout(context);
		calendar.measure(500, 450);
		calendar.layout(0, 0, 500, 450);
		calendar.setDrawingCacheEnabled(true);
		Bitmap bitmap = calendar.getDrawingCache();
		view.setImageViewBitmap(R.id.calendar, bitmap);
		appWidgetManager.updateAppWidget(appWidgetIds, view);
	}

}