package com.sweetrenard.geekcalendar.widget;

import android.content.Context;
import android.util.MonthDisplayHelper;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.GridLayout;
import android.widget.TextView;

public class MonthCell extends TextView {

	private static final String FORMAT = "0x%3X[%02X]";
	
	public MonthCell(Context context) {
		super(context);
		init(context);
	}
	
	private void init(Context context) {
		//TODO also add to style?
		setLayoutParams(new GridLayout.LayoutParams(
				GridLayout.spec(0), GridLayout.spec(0, 7, GridLayout.FILL)));
		setGravity(Gravity.CENTER);
		//setPadding(0, 0, 0, CalendarLayout.PXtoDP(context, 20));
		setHeight(CalendarLayout.PXtoDP(context, 50));
	}
	
	public static MonthCell getInstance(
			Context context, MonthDisplayHelper helper, CalendarLayout.Params params) {
		MonthCell mc = new MonthCell(context);
		mc.setTypeface(params.MONTH_FONT);
		mc.setTextColor(params.MONTH_COLOR);
		mc.setTextSize(TypedValue.COMPLEX_UNIT_PX, params.MONTH_SIZE);
		mc.setBackgroundResource(params.MONTH_BACKGROUND);
		String year = String.format(FORMAT, helper.getYear(), helper.getMonth() + 1);
		mc.setText(year);
		return mc;
	}
}
