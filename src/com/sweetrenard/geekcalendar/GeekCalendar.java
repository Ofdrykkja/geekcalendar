package com.sweetrenard.geekcalendar;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class GeekCalendar extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_geek_calendar);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_geek_calendar, menu);
		return true;
	}

}
